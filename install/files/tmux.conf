# Need to overhaul this
# Start here: https://github.com/tmux/tmux/issues/754#issuecomment-303156000

#set -g default-command "login-shell"
set -g default-shell $SHELL
set -g default-command "reattach-to-user-namespace -l ${SHELL}"

# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
# bind-key -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"

# tmux display things in 256 colors
# set -g default-terminal "tmux-256color-italic"
# set -g default-terminal "xterm-kitty"

#  enable terminal compability
# set-option -ga terminal-overrides ",xterm-256color:Tc"
# set -g default-terminal "xterm-kitty"
set -g default-terminal "xterm-256color"
# set-option -ga terminal-overrides ",xterm-kitty:Tc"
set-option -ga terminal-overrides ",xterm-256color:Tc"


set -g history-limit 20000

# automatically renumber tmux windows
set -g renumber-windows on

# unbind default prefix and set it to Ctrl+a
unbind C-b
set -g prefix C-a
bind C-a send-prefix

# Activity Monitoring
setw -g monitor-activity off
set -g visual-activity off

# Rather than constraining window size to the maximum size of any client
# connected to the *session*, constrain window size to the maximum size of any
# client connected to *that window*. Much more reasonable.
setw -g aggressive-resize on

# make delay shorter
set -sg escape-time 0

# tile all windows
unbind =
bind = select-layout tiled

# make window/pane index start with 1
set -g base-index 1
setw -g pane-base-index 1

set-option -g set-titles on
set-option -g set-titles-string "#T - #W"
# set-window-option -g automatic-rename on

######################
#### Key Bindings ####
######################

# reload config file
bind r source-file ~/.tmux.conf \; display "Config Reloaded!"

# quickly open a new window
bind N new-window

# split window and fix path for tmux 1.9
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"

# pane movement shortcuts
bind -r h select-pane -L
bind -r j select-pane -D
bind -r k select-pane -U
bind -r l select-pane -R

bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# Cycle through windows
bind -r n select-window -t :+

# Resize pane shortcuts
bind -r H resize-pane -L 10
bind -r J resize-pane -D 10
bind -r K resize-pane -U 10
bind -r L resize-pane -R 10

# enable mouse support for switching panes/windows
setw -g mouse on

bind-key -T root PPage if-shell -F "#{alternate_on}" "send-keys PPage" "copy-mode -e; send-keys PPage"
bind-key -T copy-mode-vi PPage send-keys -X page-up
bind-key -T copy-mode-vi NPage send-keys -X page-down

bind -T root WheelUpPane if-shell -F -t = "#{alternate_on}" "select-pane -t =; send-keys -M" "select-pane -t =; copy-mode -e; send-keys -M"
bind -T root WheelDownPane if-shell -F -t = "#{alternate_on}" "select-pane -t =; send-keys -M" "select-pane -t =; send-keys -M"
bind-key -T copy-mode-vi WheelUpPane send-keys -X halfpage-up
bind-key -T copy-mode-vi WheelDownPane send-keys -X halfpage-down

# maximizing and restoring windows
unbind Up
bind Up new-window -d -n fullscreen \; swap-pane -s fullscreen.1 \; select-window -t fullscreen
unbind Down
bind Down last-window \; swap-pane -s fullscreen.1 \; kill-window -t fullscreen

# set vi mode for copy mode
setw -g mode-keys vi
# more settings to make copy-mode more vim-like
unbind [
bind Escape copy-mode
unbind p
#bind p paste-buffer
bind p run "tmux set-buffer $(reattach-to-user-namespace pbpaste); tmux paste-buffer"
bind -T copy-mode-vi 'v' send-keys -X begin-selection
#bind -t vi-copy 'y' copy-selection

# Setup 'v' to begin selection as in Vim
bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "reattach-to-user-namespace pbcopy"

# Update default binding of `Enter` to also use copy-pipe
unbind -T copy-mode-vi Enter
bind -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "reattach-to-user-namespace pbcopy"


# Buffers to/from Mac clipboard, yay tmux book from pragprog
#bind C-c run "tmux save-buffer - | reattach-to-user-namespace pbcopy"
#bind C-v run "tmux set-buffer $(reattach-to-user-namespace pbpaste); tmux paste-buffer"
##############################

### Color & Style Settings ###
##############################

