" General Settings ---------------------------{{{
  set nocompatible		" not compatible with vi
  " set autoread			" detect when a file is changed

  " Setup clipboard sharing
  set clipboard+=unnamedplus
  set pastetoggle=<f6>
  set nopaste

  " enable 24 bit color support if supported
  " if (has("termguicolors"))
  "     set termguicolors
  " endif

  " set t_Co=256                " Explicitly tell vim that the terminal supports 256 colors

  set number		" Show line numbers
  set relativenumber	" Set relative line numbers

  set wrap                    " turn on line wrapping
  set wrapmargin=8            " wrap lines when coming within n characters from side
  set linebreak               " set soft wrapping
  set showbreak=…             " show ellipsis at breaking

  set autoindent              " automatically set indent of new line
  set smartindent

  " toggle invisible characters
  set list
  set listchars=tab:→\ ,eol:¬,trail:⋅,extends:❯,precedes:❮
  set showbreak=↪

  " Configure Swap
  set history=1000            " change history to 1000
  set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
  set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp

  " Tab control
  set noexpandtab             " insert tabs rather than spaces for <Tab>
  set smarttab                " tab respects 'tabstop', 'shiftwidth', and 'softtabstop'
  set tabstop=4               " the visible width of tabs
  set softtabstop=4           " edit as if the tabs are 4 characters wide
  set shiftwidth=4            " number of spaces to use for indent and unindent
  set shiftround              " round indent to a multiple of 'shiftwidth'
  set completeopt+=longest

  set ttyfast                 " faster redrawing
  " set diffopt+=vertical
  set laststatus=2            " show the satus line all the time
  set so=7                    " set 7 lines to the cursors - when moving vertical
  set wildmenu                " enhanced command line completion
  set wildmode=list:longest   " complete files like a shell
  set cmdheight=1             " command bar height
  set title                   " set terminal title

  " Searching
  set ignorecase              " case insensitive searching
  set smartcase               " case-sensitive if expresson contains a capital letter
  set hlsearch                " highlight search results
  set incsearch               " set incremental search, like modern browsers
  " set nolazyredraw            " don't redraw while executing macros
  set lazyredraw            " don't redraw while executing macros

  set magic                   " Set magic on, for regex

  " make the highlighting of tabs and other non-text less annoying
  highlight SpecialKey ctermbg=none ctermfg=8
  highlight NonText ctermbg=none ctermfg=8
  set showmatch               " show matching braces
  set mat=2                   " how many tenths of a second to blink

  if has('mouse')
      set mouse=a
      " set ttymouse=xterm2
  endif

  set noshowmode				" Let airline show status
  syntax on

"  " code folding setings
"  set foldmethod=syntax
"  set foldnestmax=10
"  set nofoldenable
"  set foldlevel=1
  let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  set termguicolors
  set undofile
  set undodir=~/.config/nvim/undofiles
  let g:black_linelength=120
" }}}

" Plugins ------------------------------------{{{
  if (!isdirectory(expand("$HOME/.config/nvim/repos/github.com/Shougo/dein.vim")))
    call system(expand("mkdir -p $HOME/.config/nvim/repos/github.com"))
    call system(expand("git clone https://github.com/Shougo/dein.vim $HOME/.config/nvim/repos/github.com/Shougo/dein.vim"))
  endif

  set runtimepath+=~/.config/nvim/repos/github.com/Shougo/dein.vim/
  call dein#begin(expand('~/.config/nvim'))

  call dein#add('Shougo/dein.vim')

  " Plugins go here
  call dein#add('scrooloose/nerdtree') " File Drawer
  call dein#add('tpope/vim-fugitive') " Git commands from vim
  call dein#add('christoomey/vim-tmux-navigator') " Navigate tmux/vim splits easily
  call dein#add('mhinz/vim-sayonara') " Kill buffers easily without closing window
  call dein#add('vim-airline/vim-airline') " Status Bar for vim
  call dein#add('mhartington/oceanic-next') " Nice vim them
  call dein#add('tmhedberg/SimpylFold', {'on_ft': 'python'}) " Python code folding
  call dein#add('airblade/vim-gitgutter') " Vim gutter statuses
  call dein#add('tpope/vim-surround')
  call dein#add('tpope/vim-repeat')
  call dein#add('tomtom/tcomment_vim')
  call dein#add('Shougo/deoplete.nvim', {'on_ft': 'python'})
  call dein#add('zchee/deoplete-jedi', {'on_ft': 'python'})
  call dein#add('benekastah/neomake')
  call dein#add('pearofducks/ansible-vim')
  call dein#add('rbgrouleff/bclose.vim')
  call dein#add('dense-analysis/ale')

  " call dein#add('francoiscabrol/ranger.vim')
  call dein#add('ambv/black')

  if dein#check_install()
    call dein#install()
    let pluginsExist=1
  endif

  call dein#end()

  filetype plugin indent on
  syntax enable

  let NERDTreeShowHidden=1
" }}}

" Keys ---------------------------------------{{{
  " Change mapleader
  let mapleader = ","

  " shortcut to save
  nmap <leader>, :w<cr>

  inoremap jk <esc>

  " Toggle NERDTree
  nmap <silent> <leader>k :NERDTreeToggle<cr>
  " expand to the path of the file in the current buffer
  "nmap <silent> <leader>y :NERDTreeFind<cr>

  " Swap between open buffers using Tab and Shift-Tab
  :nnoremap <Tab> :bnext<cr>
  :nnoremap <S-Tab> :bprevious<cr>

  " Navigate between display lines
  nnoremap <silent> j gj
  nnoremap <silent> k gk
  nnoremap <silent> ^ g^
  nnoremap <silent> $ g$

  " Replace word under cursor
  nnoremap <leader>* :%s/<c-r><c-w>//g<left><left>

  " Close the current buffer and move to the previous one
  cnoreabbrev <expr> x getcmdtype() == ":" && getcmdline() == 'x' ? 'Sayonara' : 'x'
  nmap <leader>1 <Plug>AirlineSelectTab1
  nmap <leader>2 <Plug>AirlineSelectTab2
  nmap <leader>3 <Plug>AirlineSelectTab3
  nmap <leader>4 <Plug>AirlineSelectTab4
  nmap <leader>5 <Plug>AirlineSelectTab5
  nmap <leader>6 <Plug>AirlineSelectTab6
  nmap <leader>7 <Plug>AirlineSelectTab7
  nmap <leader>8 <Plug>AirlineSelectTab8
  nmap <leader>9 <Plug>AirlineSelectTab9

  " Exit insert mode, dd line, open line above cursor
  inoremap <c-d> <esc>ddO

  " Go to beginning of line
  noremap H ^
  " Go to end of line
  noremap L g_
  " Toggle Comment
  vnoremap <C-/> :TComment<cr>
  " deoplete tab-complete
  inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

  " Unsets last search pattern by hitting return in command mode
  nnoremap <cr> :noh<cr><cr>
  " Neomake jump to next error
  nnoremap <leader>ne :ll<cr>
  cnoreabbrev <expr> x getcmdtype() == ":" && getcmdline() == 'x' ? 'Sayonara' : 'x'

  " Save as sudo
  command W :execute ':silent w !sudo tee % > /dev/null' | :edit!
" }}}

" Appearance ---------------------------------{{{
  colorscheme OceanicNext
  set background=dark

  " vim-airline settings
  let g:airline#extensions#tabline#enabled = 1
  set hidden
  let g:airline#extensions#tabline#fnamemod = ':t'
  let g:airline#extensions#tabline#show_tab_nr = 1
  let g:airline_powerline_fonts = 1
  let g:airline_theme='oceanicnext'
  " let g:airline_theme='base16_solarized'
  "nmap <leader>t :term<cr>
  "nmap <leader>, :bnext<CR>
  "nmap <leader>. :bprevious<CR>
  let g:airline#extensions#tabline#buffer_idx_mode = 1
  set guifont=Sauce\ Code\ Pro\ Nerd\ Font\ Complete:h13
" }}}

" Folds --------------------------------------{{{

  function! MyFoldText() " {{{
      let line = getline(v:foldstart)

      let nucolwidth = &fdc + &number * &numberwidth
      let windowwidth = winwidth(0) - nucolwidth - 3
      let foldedlinecount = v:foldend - v:foldstart

      " expand tabs into spaces
      let onetab = strpart('          ', 0, &tabstop)
      let line = substitute(line, '\t', onetab, 'g')

      let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
      let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
      return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . '…' . ' '
  endfunction " }}}

  set foldtext=MyFoldText()

  autocmd InsertEnter * if !exists('w:last_fdm') | let w:last_fdm=&foldmethod | setlocal foldmethod=manual | endif
  autocmd InsertLeave,WinLeave * if exists('w:last_fdm') | let &l:foldmethod=w:last_fdm | unlet w:last_fdm | endif

  autocmd FileType vim setlocal fdc=1
  set foldlevel=99
  " Space to toggle folds.
  nnoremap <Space> za
  vnoremap <Space> za
  autocmd FileType vim setlocal foldmethod=marker
  autocmd FileType vim setlocal foldlevel=0

  autocmd FileType html setlocal fdl=99

  autocmd FileType javascript,html,css,scss,typescript setlocal foldlevel=99
  autocmd FileType css,scss,json setlocal foldmethod=marker
  autocmd FileType css,scss,json setlocal foldmarker={,}

  autocmd FileType coffee setl foldmethod=indent
  autocmd FileType html setl foldmethod=expr
  autocmd FileType html setl foldexpr=HTMLFolds()

  autocmd FileType javascript,typescript,json setl foldmethod=syntax
" }}}

" Language Settings --------------------------------{{{
  " Enable Deoplete Completion at Startup
  " let g:deoplete#enable_at_startup = 1
  " Disable Deoplete preview globally
  set completeopt-=preview

  " Python Settings
  autocmd FileType python setlocal tabstop=4
  autocmd FileType python setlocal softtabstop=4
  autocmd FileType python setlocal shiftwidth=4
"  autocmd FileType python setlocal textwidth=79
  autocmd FileType python setlocal expandtab
  autocmd FileType python setlocal autoindent
  autocmd FileType python setlocal fileformat=unix
  autocmd BufWritePre *.py execute ':Black'
  " ansible
  au BufRead,BufNewFile *.yml set filetype=yaml.ansible
"  autocmd FileType python setlocal completeopt-=preview
" }}}

" Linting ------------------------------------------{{{

  let g:ale_fixers = {
  \ '*': ['remove_trailing_lines', 'trim_whitespace'],
  \ 'python': ['flake8'],
  \ 'ansible': ['ansible-lint']
  \}
  let g:ale_python_flake8_options = '--ignore=E501,E203,E266,W503 --max-line-length=120 --max-complexity=18 --select=B,C,E,F,W,T4'
  " let g:neomake_warning_sign = {'text': '∆', 'texthl': 'NeomakeWarningSign'}
  " autocmd! BufWinEnter,BufWritePost * Neomake
  " Set python locations for neomake
  " let g:python_host_prog = '/usr/local/bin/python3'
  " let g:python2_host_prog = '/usr/local/bin/python3'
  " let g:python3_host_prog = '/usr/local/bin/python3'

  " Ignore PEP E501 (line length) checking
  " let g:neomake_python_pylama_maker = {
  "     \ 'args': ['--format', 'pep8', '--ignore', 'E501'],
  "     \ 'errorformat': '%f:%l:%c: %t%m',
  "     \ }
" }}}
