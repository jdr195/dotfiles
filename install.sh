#!/bin/sh

cd install
source bootstrap.sh 
ansible-galaxy install -r requirements.yml
ansible-playbook site.yml --ask-become-pass
